CS3505
FFMPEG
Spring 2014
Daryl Bennett | Christopher Johnson

Our goal was to:
*Create our own extension named "XKCD". (Named after the famous comics)
	-We created an encoder and a decoder named, xkcdenc & xkcddec. This means we could take images 
         such as JPG images and encode them to xkcd, and vice versa.
	-The challenge was working with such a large code base, and where to put the files in FFMPEG, and what
         existing files had to be configured to make our custom extension to work. FFMPEG is large, and so instead 
         of including all of the code, I have only included key files.
	-Here's the shell command that converts a JPG into an XKCD file, and displays the image
		-ffmpeg/ffmpeg -i neuschwanstein.jpg neuschwanstein2.xkcd && ffmpeg/ffplay neuschwanstein2.xkcd
	
*Create an animation
    -We created a short animation by creating 300 images with a ball bouncing up and down, and creating 
	 a video of the ball bouncing. 
	-bouncer.c is the file that accomplishes this.