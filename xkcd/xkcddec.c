/* CS 3505, Spring 2014 */
/* Christopher S. Johnson and Daryl S. Bennett */
/* Project 1, Milestone 2 */

/* Based with revisions on
 * BMP image format decoder
 * Copyright (c) 2005 Mans Rullgard
 */

#include "avcodec.h"
#include "bytestream.h"
#include "internal.h"
#include "msrledec.h"

#define DEPTH 8 // bits per pixel

static int
xkcd_decode_frame(AVCodecContext *avctx,
                  void *data,
		  int *got_frame,
                  AVPacket *avpkt)
{
    const uint8_t *buf = avpkt->data; // output image
    int buf_size       = avpkt->size; // size of buffer
    AVFrame *frame     = data;        // input image
    unsigned int fsize; // total file size of input image
    unsigned int hsize; // total header size of input image
    int width; // width in pixels of input/output image
    int height; // height in pixels of input/output image
    unsigned int ihsize; // info header size
    int i; // generic loop counter
    int n_bytes_per_row; // number of bytes in a row (not including padding)
    int linesize; // size of an image line with padding in XKCD file
    int ret_err; // error stashing variable
    uint8_t *ptr; // ptr to input data
    int dsize; // size of image data not including headers in XKCD file
    const uint8_t *buf0 = buf; // output data start that doesn't change

    if (buf_size < 14)
    {
        av_log(avctx, AV_LOG_ERROR, "buf size too small (%d)\n", buf_size);
        return AVERROR_INVALIDDATA;
    }

    if (bytestream_get_byte(&buf) != 'X' ||
        bytestream_get_byte(&buf) != 'K' ||
	bytestream_get_byte(&buf) != 'C' ||
	bytestream_get_byte(&buf) != 'D')
    {
        av_log(avctx, AV_LOG_ERROR, "bad magic number\n");
        return AVERROR_INVALIDDATA;
    }

    fsize = bytestream_get_le32(&buf); // same as n_bytes in encoder
    if (buf_size < fsize)
    {
        av_log(avctx,
	       AV_LOG_ERROR,
	       "not enough data (%d < %d), trying to decode anyway\n",
               buf_size,
	       fsize);
        fsize = buf_size;
    }

    hsize  = bytestream_get_le32(&buf); /* header size */
    ihsize = bytestream_get_le32(&buf); /* more header size */
    if (ihsize + 14 > hsize)
    {
        av_log(avctx, AV_LOG_ERROR, "invalid header size %d\n", hsize);
        return AVERROR_INVALIDDATA;
    }
    
    /* sometimes file size is set to some headers size,
     * set a real size in that case */
    
    if (fsize <= hsize)
    {
        av_log(avctx,
	       AV_LOG_ERROR,
	       "declared file size is less than header size (%d < %d)\n",
               fsize,
	       hsize);
        return AVERROR_INVALIDDATA;
    }
    
    avctx->width = width = bytestream_get_le32(&buf);
    avctx->height = height = bytestream_get_le32(&buf);
    
    avctx->pix_fmt = AV_PIX_FMT_RGB8;
        
    if ((ret_err = ff_get_buffer(avctx, frame, 0)) < 0)
        return ret_err;
    frame->pict_type = AV_PICTURE_TYPE_I;
    frame->key_frame = 1;

    buf   = buf0 + hsize; // skip header
    dsize = buf_size - hsize; // size of image data, not including headers

    /* Line size in file multiple of 4 */
    n_bytes_per_row = ((avctx->width * DEPTH + 31) / 8) & ~3;
    
    if (n_bytes_per_row * avctx->height > dsize)
    {
        av_log(avctx, AV_LOG_ERROR, "not enough data (%d < %d)\n",
               dsize, n_bytes_per_row * avctx->height);
        return AVERROR_INVALIDDATA;
    }
    
    ptr      = frame->data[0];
    linesize = frame->linesize[0];
    
    for (i = 0; i < avctx->height; i++)
    {
	memcpy(ptr, buf, n_bytes_per_row);
	buf += n_bytes_per_row;
	ptr += linesize;
    }

    *got_frame = 1;

    return buf_size;
}

AVCodec
ff_xkcd_decoder = {
    .name           = "xkcd",
    .long_name      = NULL_IF_CONFIG_SMALL("XKCD bitmap"),
    .type           = AVMEDIA_TYPE_VIDEO,
    .id             = AV_CODEC_ID_XKCD,
    .decode         = xkcd_decode_frame,
    .capabilities   = CODEC_CAP_DR1,
};
