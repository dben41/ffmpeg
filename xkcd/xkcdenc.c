/* CS 3505, Spring 2014 */
/* Christopher S. Johnson and Daryl S. Bennett */
/* Project 1, Milestone 2 */

/* Based with revisions on
 * BMP image format encoder
 * Copyright (c) 2006, 2007 Michel Bardiaux
 * Copyright (c) 2009 Daniel Verkamp <daniel at drv.nu>
 */

#include "libavutil/imgutils.h"
#include "libavutil/avassert.h"
#include "avcodec.h"
#include "bytestream.h"
#include "internal.h"


#define SIZE_XKCDFILEHEADER 12
#define SIZE_XKCDINFOHEADER 12


static av_cold int
xkcd_encode_init(AVCodecContext *avctx)
{
    avctx->bits_per_coded_sample = 8;  //we want 8
    avctx->coded_frame = av_frame_alloc();
    if (!avctx->coded_frame)
        return AVERROR(ENOMEM);
    else
        return 0; // no error
}


static int
xkcd_encode_frame(AVCodecContext *avctx,
		  AVPacket *pkt,
		  const AVFrame * const pict,
		  int *got_packet)
{
    int n_bytes_image; // bytes used in image portion, including padding
    int n_bytes_per_row; // bytes in one image row (not including padding)
    int n_bytes; // total bytes in file
    int i; // generic counter variable
    int hsize; // hsize is the size of the header
    int ret_err; // gets an error code later on to be passed through if error

    uint32_t palette256[256]; // color palette of 4 bytes for each color
    // padding bytes for each row to reach multiple of 4 byets
    int pad_bytes_per_row;
    int pal_entries = 0; // number of colors in palette (will be 256)
    int bit_count = avctx->bits_per_coded_sample; // bits per pixel
    uint8_t *ptr; // will point to start of a line in source image
    uint8_t *buf; // will point to start of a line in destination image

    avctx->coded_frame->pict_type = AV_PICTURE_TYPE_I;
    avctx->coded_frame->key_frame = 1;

    // initialize the palette
    avpriv_set_systematic_pal2(palette256, avctx->pix_fmt);

    pal_entries = 256; // 256 colors in palette

    // from en.wikipedia.org/wiki/BMP_file_format
    n_bytes_per_row = (((int64_t)avctx->width *
		        (int64_t)bit_count + 31LL)
	               / 8) & ~3;
    
    pad_bytes_per_row = (4 - n_bytes_per_row) & 3;
    n_bytes_image = avctx->height * (n_bytes_per_row + pad_bytes_per_row);

    // hsize is the size of the header
    hsize = SIZE_XKCDFILEHEADER + SIZE_XKCDINFOHEADER + (pal_entries << 2);
    n_bytes = n_bytes_image + hsize;
    
    if ((ret_err = ff_alloc_packet2(avctx, pkt, n_bytes)) < 0)
        return ret_err;
    buf = pkt->data;
    
    // XKCDFILEHEADER.bfType
    bytestream_put_byte(&buf, 'X');
    bytestream_put_byte(&buf, 'K');
    bytestream_put_byte(&buf, 'C');
    bytestream_put_byte(&buf, 'D');

    // file size
    bytestream_put_le32(&buf, n_bytes);
    // header size
    bytestream_put_le32(&buf, hsize);
     // info header size
    bytestream_put_le32(&buf, SIZE_XKCDINFOHEADER);
    // width
    bytestream_put_le32(&buf, avctx->width);
    // height
    bytestream_put_le32(&buf, avctx->height);
    
    for (i = 0; i < pal_entries; i++)
        bytestream_put_le32(&buf, palette256[i] & 0xFFFFFF);
    
    // not anymore // XKCD files are bottom-to-top so we start from the end...
    //ptr = pict->data[0] + (avctx->height - 1) * pict->linesize[0];
    ptr = pict->data[0];
    buf = pkt->data + hsize;
    for (i = 0; i < avctx->height; i++)
    {
        memcpy(buf, ptr, n_bytes_per_row);
	buf += n_bytes_per_row;
        memset(buf, 0, pad_bytes_per_row);
        buf += pad_bytes_per_row;
        //ptr -= pict->linesize[0]; // ... and go back
	ptr += pict->linesize[0];
    }

    pkt->flags |= AV_PKT_FLAG_KEY;
    *got_packet = 1;
    return 0; // no error
}


static av_cold int
xkcd_encode_close(AVCodecContext *avctx)
{
    av_frame_free(&avctx->coded_frame);
    return 0;
}

AVCodec
ff_xkcd_encoder = {
    .name           = "xkcd",
    .long_name      = NULL_IF_CONFIG_SMALL("XKCD bitmap"),
    .type           = AVMEDIA_TYPE_VIDEO,
    .id             = AV_CODEC_ID_XKCD,
    .init           = xkcd_encode_init,
    .encode2        = xkcd_encode_frame,
    .close          = xkcd_encode_close,
    .pix_fmts       = (const enum AVPixelFormat[]){
        AV_PIX_FMT_RGB8,
	AV_PIX_FMT_NONE
    },
};
