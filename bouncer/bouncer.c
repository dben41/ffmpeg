#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/avutil.h"
#include "libswscale/swscale.h"

/*
 *Converts an AVFrame to a new AVFrame
 */
static AVFrame*
convert(AVFrame *input_frame, int format)
{
  //bytes in the frame
  int                     numBytes; 
  //struct context           
  struct SwsContext      *sws_ctx = NULL;
  //frame
  AVFrame                *pFrameRGB = NULL;

  uint8_t                *buffer = NULL;

  int width = input_frame->width;
  int height = input_frame->height;  

  // Determine required buffer size and allocate buffer
  numBytes = avpicture_get_size(format,
				input_frame->width,
				input_frame->height);
  buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));

  sws_ctx = sws_getContext(input_frame->width,
			   input_frame->height,
			   input_frame->format,
			   input_frame->width,
			   input_frame->height, 
			   format,
			   SWS_BILINEAR,
			   NULL,
			   NULL,
			   NULL);
 
  // Allocate an AVFrame structure
  pFrameRGB = av_frame_alloc();
  if (!pFrameRGB)
    return NULL;

  // Assign appropriate parts of buffer to image planes in pFrameRGB
  // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
  // of AVPicture
  avpicture_fill((AVPicture *)pFrameRGB,
		 buffer,
		 format,
		 input_frame->width,
		 input_frame->height);
  
  // Convert the image from its native format to RGB
  sws_scale(sws_ctx,
	    (uint8_t const * const *)input_frame->data,
	    input_frame->linesize,
	    0,
	    input_frame->height,
	    pFrameRGB->data,
	    pFrameRGB->linesize);

  //get dimensions
  pFrameRGB->height = input_frame->height;
  pFrameRGB->width = input_frame->width;
  pFrameRGB->format = format;

  return pFrameRGB; //tentative return
} // convert














/*
 *Loads frame from a filename
 *takes in a file name and returns an AVFrame
 */
static AVFrame*
load_frame(char* filename)
{
  //Declare vars
  AVFormatContext *pFormatCtx = NULL;
  int i, videoStream;
  AVCodecContext *pCodecCtx = NULL;
  AVCodec *pCodec = NULL;
  AVFrame *pFrame = NULL;
  AVFrame *pFrameRGB = NULL;
  AVPacket packet;
  int frameFinished;

  AVDictionary    *optionsDict = NULL;

  printf("filename is: \"%s\"\n", filename);
  
  // Open video file
  if (avformat_open_input(&pFormatCtx, filename, NULL, NULL) != 0)
    return NULL; // Couldn't open file
  
  // Retrieve stream information
  if (avformat_find_stream_info(pFormatCtx, NULL) < 0)
    return NULL; // Couldn't find stream information
  
  // Dump information about file onto standard error
  av_dump_format(pFormatCtx, 0, filename, 0);
  
  // Find the first video stream

  printf("number of streams is: %d\n", pFormatCtx->nb_streams);
  
  videoStream = -1;
  for (i = 0; i < pFormatCtx->nb_streams; i++)
  {
    if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
    {
      videoStream = i;
      break;
    }
  }
  if (videoStream == -1)
    return NULL; // Didn't find a video stream

  printf("videoStream is: %d\n", videoStream);
  printf("pFormatCtx->streams[0]->codec->codec_type is: %d\n",
	 pFormatCtx->streams[0]->codec->codec_type);

  // Get a pointer to the codec context for the video stream
  pCodecCtx = pFormatCtx->streams[videoStream]->codec;
  printf("pointer pCodecCtx is: 0x%X\n", pCodecCtx);
  
  // Find the decoder for the video stream
  pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
  printf("pointer pCodec is: 0x%X\n", pCodec);
  if (!pCodec)
  {
    fprintf(stderr, "Unsupported codec!\n");
    return NULL; // Codec not found
  }
  // Open codec
  if (avcodec_open2(pCodecCtx, pCodec, &optionsDict) < 0)
    return NULL; // Could not open codec
  
  // Allocate video frame
  pFrame = av_frame_alloc();
  printf("pointer pFrame is: 0x%X\n", pFrame);
  
  //Read frames and save to disk
  i = 0;
  while (av_read_frame(pFormatCtx, &packet) >= 0) 
  {
    // Is this a packet from the video stream?
    if (packet.stream_index == videoStream) 
    {
      // Decode video frame
      avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
      // Did we get a video frame?
      if (frameFinished)
        pFrameRGB = convert(pFrame,AV_PIX_FMT_RGB24);
    //increment i?
    }
  }
  
  return pFrameRGB;
} // load_frame

/*
 *Writes the frame with the circle
 *Index is the number of the file
 */
static int
write_frame(AVFrame* input_frame, int index)
{
  FILE *output;
  AVCodec *codec;
  AVFrame *temp;
  AVPacket pkt;
  int got_output; //verifies if we have gotten output

  //name the file xkcd
  char output_fname[14];
  int chars_written = snprintf(output_fname, 14, "frame%03d.xkcd", index);
  if (chars_written != 13)
  {
    fprintf(stderr,
	    "chars_written from snprintf returned %d instead of 14!\n",
	    chars_written);
    fprintf(stderr, "len of output_fname is: %d\n", strlen(output_fname));
    fprintf(stderr, "output_fname is: \"%s\"\n\n", output_fname);
    return -1;
  }
  //sprintf();  //TODO this is where i name it? 
  output = fopen(output_fname, "wb");
  //error check 
  if (!output)
  {
    fprintf(stderr, "fopen failed!\n");
    return -1;
  }
    
  //init the packet
  av_init_packet(&pkt);
  
  //get the xkcd codec
  codec = avcodec_find_encoder(AV_CODEC_ID_XKCD);
  if (!codec)
  {
    fprintf(stderr, "avcodec_find_encoder failed!\n");
    return -1;
  }

  AVCodecContext *cxt = avcodec_alloc_context3(codec); //c
  if (!cxt)
  {
    fprintf(stderr, "avcodec_alloc_context3 returned NULL\n");
    return -1;
  }
  
  //set context vars
  cxt->height = input_frame->height;
  cxt->width = input_frame->width;
  //Peter showed us this
  cxt->pix_fmt = codec->pix_fmts[0];
  
  //open
  AVDictionary    *optionsDict = NULL;
  if (avcodec_open2(cxt, codec, &optionsDict) < 0)
  {
    fprintf(stderr, "avcodec_open2 failed!\n");
    return -1; // Could not open codec
  }
  
  // Allocate video frame
  temp = av_frame_alloc();
  //frame
  temp = convert(input_frame, codec->pix_fmts[0]);
     
  //encode image  
  if (avcodec_encode_video2(cxt, &pkt, temp, &got_output) < 0)
  {   
    fprintf(stderr, "avcodec_encode_video2 failed!\n");
    return -1; // Could not encode video!
  }
  
  //write
  if (got_output)
    fwrite(pkt.data, 1, pkt.size, output);
  
  //free and close
  av_frame_free(&temp);
  //     av_free(temp);

  
  return 0;
}

/*/typedef struct
{
  //lower limit
  //upper limit
  
}circle_constants;
*/
void
draw_pixel(AVFrame *f,
	   int x,
	   int y,
	   int cx,
	   int cy,
	   int rad)
{
  uint8_t *line = f->data[0] + f->linesize[0] * y;
  uint8_t *pixel = line + 3 * x;
  
  int x_disp = cx - x;
  int y_disp = cy - y;
  float distance = sqrtf((float)(x_disp * x_disp + y_disp * y_disp));
  uint8_t red = (uint8_t) (255 * (rad - distance) / rad);
  pixel[0] = red;
  pixel[1] = 0;
  pixel[2] = 0;
}



void
draw_circle(AVFrame *f, int cx, int cy)
{
  unsigned rad = f->height*.20;
  int vdisp = 0;
  int y;
  for (y = 0; y < f->height; y++)
  {
    int x;
    for (x = 0; x < f->width; x++)
    {
      int dx = cx - x;
      int dy = cy - y;
      
      if (dx * dx + dy * dy <= rad * rad)
	draw_pixel(f, x, y, cx, cy, rad); // cx, cy, rad);
    }
  }
}

/*
 *This is the main driver method, that makes 300 copies.
 *
 */
int
main(int argc, char **argv)
{
  //get filename
  char *input_filename = argv[1];
  //av_log(NULL, AV_LOG_INFO, "Successfully linked to ffmpeg.\n");
  //printf("Start of main.\n");
  if (argc != 2)
  {
    fprintf(stderr, "Must pass in a single filename argument.\n");
    return 1;
  }
  assert(argc == 2);
  //verify is jpg

  //register all
  av_register_all();
  //frame, load frame, this is the const
  AVFrame *pFrame = load_frame(input_filename);
  if (!pFrame)
  {
    fprintf(stderr, "load_frame returned NULL!\n");
    return 1;
  }
  
  //Circle info
  int cfx = pFrame->width*.5; // center of picture, x coor, fix
  int cfy = pFrame->height*.5; // center of picture, y coor, fix
  int cx = pFrame->width*.5; // center of the ball, x coor, dynamic
  int cy = pFrame->height*.5; // center of the pictureball, y coor, dynamic
  int upper_limit = pFrame->height*.3;//upper limit its inverted (image top)
  int lower_limit = pFrame->height*.7;//lower limit (image bottom)
  int speed = 8;//speed
  
  //this is the index that helps us keep track of which file
  int i;
  //write 300 files
  for (i = 0; i < 300; i++)
  {
    //new copy, maybe name it here?
    AVFrame *copy_frame = convert(pFrame, AV_PIX_FMT_RGB24);
    if (!copy_frame)
    {
      fprintf(stderr, "convert returned NULL\n");
      return 1;
    }
    //determine whether ball goes up or down
    if(cy <= upper_limit || cy >= lower_limit)
      speed = -speed;
    
    //modify y-axis
    cy += speed;
    
    // do drawing!! ahhhhhhhhhh
    draw_circle(copy_frame, cx, cy);
    
    //create circle
    write_frame(copy_frame, i);

    // clean up
    av_frame_free(&copy_frame);
  }
  //return
  return 0;
}

